from accounts.serializers import UserSerializer
from django.contrib.auth.models import User
from rest_framework import generics

from rest_framework.permissions import AllowAny


class UserCreateView(generics.CreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [AllowAny]
