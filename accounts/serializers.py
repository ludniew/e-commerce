from rest_framework import serializers
from django.contrib.auth.models import User

from accounts.models import Account
from ordering_handler.models import Order

from datetime import datetime


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ["email", "username", "password"]

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data["username"],
            email=validated_data["email"],
            password=validated_data["password"],
        )
        account = Account(
            user=user,
        )
        account.save()

        order = Order.objects.create(
            account=account,
            order_date=datetime.now(),
            additional_info=f"""
            This is super special order for user: {user}
            """,
        )

        order.save()

        return user
