import pytest

from ordering_handler.models import Order, OrderRequest


@pytest.mark.django_db
def test_order_created_correctly(order_factory):
    order = order_factory.create()

    db_order = Order.objects.get(pk=order.pk)

    assert order.account == db_order.account
    assert order.order_products == db_order.order_products
    assert order.order_date == str(db_order.order_date)
    assert order.additional_info == db_order.additional_info


@pytest.mark.django_db
def test_str_order_should_return_objects_representation(order_factory):
    order = order_factory.create()
    expected_result = f"Order(owner={order.account.user.id})"

    assert str(order) == expected_result


@pytest.mark.django_db
def test_order_request_created_correctly(order_request_factory):
    order_request = order_request_factory.create()

    order_request_db = OrderRequest.objects.all().first()

    assert order_request.order == order_request_db.order
    assert order_request.product == order_request_db.product
