import pytest
import json
from rest_framework.reverse import reverse

from ordering_handler.models import OrderRequest, Order
from products_handler.models import Product


@pytest.mark.django_db
def test_post_order_request_create_view_should_create_order_request(
    authenticated_client, product_factory, order_factory, account_factory
):
    account = account_factory.create(user=authenticated_client["user"])
    order = order_factory.create(account=account)
    expected_status_code = 201
    product = product_factory.create()
    data = {"product": product.id}

    response = authenticated_client["client"].post(
        path=reverse("make-order"), data=data
    )
    order_request_db = OrderRequest.objects.get(order=order)

    assert response.status_code == expected_status_code
    assert response.data.get("user_id") == account.user.id
    assert response.data.get("order_request_id") == order_request_db.id
    assert response.data.get("product_id") == product.id
    assert response.data.get("order_id") == order.id


@pytest.mark.django_db
def test_get_order_request_create_view_should_reach_405_not_allowed(
    authenticated_client,
):
    expected_status_code = 405
    expected_error_message = 'Method "GET" not allowed.'

    response = authenticated_client["client"].get(path=reverse("make-order"))

    assert response.status_code == expected_status_code
    assert response.data.get("detail") == expected_error_message


@pytest.mark.django_db
def test_post_order_request_create_view_should_raise_validation_error_when_list_provided(
    authenticated_client,
):
    expected_exception = AttributeError
    data = [{}]

    with pytest.raises(expected_exception):
        authenticated_client["client"].post(path=reverse("make-order"), data=data)


@pytest.mark.django_db
def test_delete_order_request_destroy_view_should_delete_order_request(
    authenticated_client, order_request_factory
):
    expected_status_code = 204
    order_request = order_request_factory.create()

    response = authenticated_client["client"].delete(
        path=reverse("delete-order", kwargs={"pk": order_request.id})
    )
    order_requests_db = OrderRequest.objects.all()

    assert response.status_code == expected_status_code
    assert len(order_requests_db) == 0


@pytest.mark.django_db
def test_delete_order_request_destroy_view_reaches_404_when_no_request_with_id(
    authenticated_client,
):
    expected_status_code = 404
    invelid_order_request_id = 1

    response = authenticated_client["client"].delete(
        path=reverse("delete-order", kwargs={"pk": invelid_order_request_id})
    )

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_get_order_retrieve_view_should_return_order_data(
    authenticated_client, order_factory
):
    order = order_factory.create()
    expected_status_code = 200

    response = authenticated_client["client"].get(
        path=reverse("order-details", kwargs={"pk": order.id})
    )
    products = Product.objects.filter(orderrequest__order=order)

    assert response.status_code == expected_status_code
    assert response.data.get("id") == order.id
    assert response.data.get("order_products") == list(products)
    assert response.data.get("order_date") == order.order_date
    assert response.data.get("additional_info") == order.additional_info


@pytest.mark.django_db
def test_get_order_retrieve_view_reaches_404_when_no_request_with_id(
    authenticated_client,
):
    expected_status_code = 404
    invelid_order_request_id = 1

    response = authenticated_client["client"].get(
        path=reverse("order-details", kwargs={"pk": invelid_order_request_id})
    )

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_get_order_list_view_should_return_user_orders_data(
    authenticated_client, order_factory, order_request_factory, account_factory
):
    expected_status_code = 200
    account = account_factory.create(user=authenticated_client["user"])
    order = order_factory.create(account=account)
    order_requests = [order_request_factory.create(order=order) for _ in range(5)]
    products = [order_request.product.id for order_request in order_requests]

    response = authenticated_client["client"].get(path=reverse("list-orders"))
    response_data = response.data[0]
    order_db = Order.objects.get(account=account)

    assert response.status_code == expected_status_code
    assert response_data.get("id") == order_db.id
    assert response_data.get("order_products") == products
    assert str(response_data.get("order_date")) == str(order_db.order_date)
    assert response_data.get("additional_info") == order_db.additional_info


@pytest.mark.django_db
def test_get_order_finalize_should_return_order_data_and_clear_products(
    authenticated_client, account_factory, order_factory, order_request_factory
):
    expected_status_code = 200
    account = account_factory.create(user=authenticated_client["user"])
    order = order_factory.create(account=account)
    order_requests = [order_request_factory.create(order=order) for _ in range(5)]
    products = [order_request.product.id for order_request in order_requests]

    response = authenticated_client["client"].get(
        path=reverse("finalize-order", kwargs={"pk": order.pk})
    )
    response_data = json.loads(response.content)

    assert response.status_code == expected_status_code
    assert response_data.get("result") == "Success"
    assert response_data.get("products_ordered") == products
