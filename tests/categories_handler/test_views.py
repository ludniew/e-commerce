import pytest
from faker import Faker
from rest_framework.test import APIClient

from categories_handler.models import Category

valid_url = "/api/v1/categories/"
fake = Faker()


@pytest.mark.django_db
def test_get_categories_reaches_401_not_authenticated(client):
    expected_status_code = 401
    expected_error_message = "Authentication credentials were not provided."

    response = client.get(path=valid_url)

    assert response.status_code == expected_status_code
    assert response.data["detail"] == expected_error_message


@pytest.mark.django_db
def test_post_categories_reaches_401_not_authenticated(client):
    expected_status_code = 401
    expected_error_message = "Authentication credentials were not provided."

    response = client.post(path=valid_url)

    assert response.status_code == expected_status_code
    assert response.data["detail"] == expected_error_message


@pytest.mark.django_db
def test_patch_categories_reaches_401_not_authenticated(client):
    expected_status_code = 401
    expected_error_message = "Authentication credentials were not provided."

    response = client.patch(path=valid_url)

    assert response.status_code == expected_status_code
    assert response.data["detail"] == expected_error_message


@pytest.mark.django_db
def test_delete_categories_reaches_401_not_authenticated(client):
    expected_status_code = 401
    expected_error_message = "Authentication credentials were not provided."

    response = client.delete(path=valid_url)

    assert response.status_code == expected_status_code
    assert response.data["detail"] == expected_error_message


@pytest.mark.django_db
def test_get_products_reaches_200_after_successful_authentication(authenticated_client):
    expected_status_code = 200

    response = authenticated_client["client"].get(path=valid_url)

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_post_products_reaches_201_after_successful_authentication(
    authenticated_client,
):
    expected_status_code = 201
    new_category_data = {
        "name": fake.word(),
    }

    response = authenticated_client["client"].post(
        path=valid_url, data=new_category_data
    )

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_patch_products_reaches_200_after_successful_authentication(
    authenticated_client, category_factory
):
    category = category_factory.create()
    expected_status_code = 200
    new_category_data = {
        "name": fake.word(),
    }

    response = authenticated_client["client"].patch(
        path=f"{valid_url}{category.id}", data=new_category_data
    )

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_delete_products_reaches_204_after_successful_authentication(
    authenticated_client, category_factory
):
    category = category_factory.create()
    expected_status_code = 204

    response = authenticated_client["client"].delete(path=f"{valid_url}{category.id}")

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_get_categories_should_return_proper_count_of_categories(
    authenticated_client, category_factory
):
    categories = [category_factory.create() for _ in range(5)]

    authenticated_client["client"].get(path=valid_url)
    categories_db_count = Category.objects.all().count()

    assert len(categories) == categories_db_count


@pytest.mark.django_db
def test_get_categories_on_invalid_url_should_return_404_not_found(
    authenticated_client,
):
    invalid_url = "/api/v1/categories/all/"
    expected_status_code = 404

    response = authenticated_client["client"].get(path=invalid_url)

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_get_specific_category_should_return_category(
    authenticated_client, category_factory
):
    category = category_factory.create()
    expected_status_code = 200

    response = authenticated_client["client"].get(path=f"{valid_url}{category.id}")

    assert response.status_code == expected_status_code
    assert response.data["id"] == category.id
    assert response.data["name"] == category.name
    assert response.data["parent_category"] == category.parent_category


@pytest.mark.django_db
def test_get_specific_category_should_return_404_when_no_category(authenticated_client):
    invalid_category_id = 1
    expected_status_code = 404
    expected_message = "Not found."

    response = authenticated_client["client"].get(
        path=f"{valid_url}{invalid_category_id}"
    )

    assert response.status_code == expected_status_code
    assert response.data["detail"] == expected_message


@pytest.mark.django_db
def test_post_with_valid_data_should_create_new_categoryy(
    authenticated_client, category_factory
):
    category = category_factory.create()
    expected_status_code = 201
    new_category_data = {
        "name": fake.word(),
        "parent_category": category.id,
    }

    response = authenticated_client["client"].post(
        path=valid_url, data=new_category_data
    )
    category_db = Category.objects.filter(pk__gt=1).first()

    assert response.status_code == expected_status_code
    assert category_db.name == new_category_data["name"]
    assert category_db.parent_category.id == new_category_data["parent_category"]


@pytest.mark.django_db
def test_post_with_missing_parent_category_should_set_default_none(
    authenticated_client, category_factory
):
    expected_status_code = 201
    new_category_data = {
        "name": fake.word(),
    }

    response = authenticated_client["client"].post(
        path=valid_url, data=new_category_data
    )
    category_db = Category.objects.all().first()

    assert response.status_code == expected_status_code
    assert category_db.name == new_category_data["name"]
    assert category_db.parent_category is None


@pytest.mark.django_db
def test_post_with_missing_name_reaches_status_code_400(
    authenticated_client, category_factory
):
    expected_status_code = 400
    new_category_data = {}
    expected_error_field = "name"
    expected_error_message = "This field is required."

    response = authenticated_client["client"].post(
        path=valid_url, data=new_category_data
    )

    assert response.status_code == expected_status_code
    assert str(response.data[expected_error_field][0]) == expected_error_message


@pytest.mark.django_db
def test_patch_updates_category_name(authenticated_client, category_factory):
    category = category_factory.create()
    updated_product_data = {
        "name": fake.word(),
    }
    expected_status_code = 200

    response = authenticated_client["client"].patch(
        path=f"{valid_url}{category.id}", data=updated_product_data
    )

    assert response.status_code == expected_status_code
    assert response.data["name"] == updated_product_data["name"]


@pytest.mark.django_db
def test_patch_updates_category_parent_category(authenticated_client, category_factory):
    parent_category = category_factory.create()
    category = category_factory.create()
    updated_product_data = {
        "parent_category": parent_category.id,
    }
    expected_status_code = 200

    response = authenticated_client["client"].patch(
        path=f"{valid_url}{category.id}", data=updated_product_data
    )

    assert response.status_code == expected_status_code
    assert response.data["parent_category"] == updated_product_data["parent_category"]


@pytest.mark.django_db
def test_patch_updates_category_data(authenticated_client, category_factory):
    parent_category = category_factory.create()
    category = category_factory.create()
    updated_product_data = {
        "name": fake.word(),
        "parent_category": parent_category.id,
    }
    expected_status_code = 200

    response = authenticated_client["client"].patch(
        path=f"{valid_url}{category.id}", data=updated_product_data
    )

    assert response.status_code == expected_status_code
    assert response.data["parent_category"] == updated_product_data["parent_category"]
    assert response.data["name"] == updated_product_data["name"]


@pytest.mark.django_db
def test_delete_deletes_product(authenticated_client, category_factory):
    categories = [category_factory.create() for _ in range(5)]

    authenticated_client["client"].delete(path=f"{valid_url}{categories[0].id}")
    categories_count_db = Category.objects.all().count()

    assert categories_count_db == len(categories) - 1


@pytest.mark.django_db
def test_delete_specific_product_returns_404_when_no_product(authenticated_client):
    invalid_category_id = 1
    expected_status_code = 404
    expected_message = "Not found."

    response = authenticated_client["client"].delete(
        path=f"{valid_url}{invalid_category_id}"
    )

    assert response.status_code == expected_status_code
    assert str(response.data["detail"]) == expected_message
