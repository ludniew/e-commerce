import pytest
from categories_handler.models import Category


@pytest.mark.django_db
def test_category_created(category_factory):
    category = category_factory.create()
    categories_db = Category.objects.all()
    category_db = categories_db.get(pk=category.pk)

    assert category_db.name == category.name
    assert category_db.parent_category == category.parent_category


@pytest.mark.django_db
def test_str_category_should_return_category_name(category_factory):
    category = category_factory.create()
    category_db = Category.objects.all().get(pk=category.pk)

    assert str(category_db) == category.name
