import factory
from faker import Faker

from django.contrib.auth.models import User

from accounts.models import Account
from categories_handler.models import Category
from ordering_handler.models import Order, OrderRequest
from products_handler.models import Product

fake = Faker()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda x: fake.user_name())
    password = factory.Sequence(lambda x: fake.password())
    email = factory.Sequence(lambda x: fake.email())
    is_staff = False
    is_active = True


class AccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Account

    user = factory.SubFactory(UserFactory)


class OrderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Order

    account = factory.SubFactory(AccountFactory)
    order_date = factory.Sequence(lambda x: fake.date())
    additional_info = factory.Sequence(lambda x: fake.text())


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category

    name = factory.Sequence(lambda x: fake.word())
    parent_category = None


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product

    name = factory.Sequence(lambda x: fake.word())
    category = factory.SubFactory(CategoryFactory)


class OrderRequestFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = OrderRequest

    order = factory.SubFactory(OrderFactory)
    product = factory.SubFactory(ProductFactory)
