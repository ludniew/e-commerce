import pytest
from faker import Faker

from rest_framework.test import APIClient

from products_handler.models import Product

valid_url = "/api/v1/products/"
fake = Faker()


@pytest.mark.django_db
def test_get_products_reaches_401_not_authenticated(client):
    expected_status_code = 401
    expected_error_message = "Authentication credentials were not provided."

    response = client.get(path=valid_url)

    assert response.status_code == expected_status_code
    assert response.data["detail"] == expected_error_message


@pytest.mark.django_db
def test_post_products_reaches_401_not_authenticated(client):
    expected_status_code = 401
    expected_error_message = "Authentication credentials were not provided."

    response = client.post(path=valid_url)

    assert response.status_code == expected_status_code
    assert response.data["detail"] == expected_error_message


@pytest.mark.django_db
def test_patch_products_reaches_401_not_authenticated(client):
    expected_status_code = 401
    expected_error_message = "Authentication credentials were not provided."

    response = client.patch(path=valid_url)

    assert response.status_code == expected_status_code
    assert response.data["detail"] == expected_error_message


@pytest.mark.django_db
def test_delete_products_reaches_401_not_authenticated(client):
    expected_status_code = 401
    expected_error_message = "Authentication credentials were not provided."

    response = client.delete(path=valid_url)

    assert response.status_code == expected_status_code
    assert response.data["detail"] == expected_error_message


@pytest.mark.django_db
def test_get_products_reaches_200_after_successful_authentication(authenticated_client):
    expected_status_code = 200

    response = authenticated_client["client"].get(path=valid_url)

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_post_products_reaches_201_after_successful_authentication(
    authenticated_client, category_factory
):
    category = category_factory.create()
    new_product_data = {
        "name": fake.word(),
        "category": category.id,
    }
    expected_status_code = 201

    response = authenticated_client["client"].post(
        path=valid_url, data=new_product_data
    )

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_patch_products_reaches_200_after_successful_authentication(
    authenticated_client, product_factory
):
    product = product_factory.create()
    expected_status_code = 200
    new_product_data = {
        "name": fake.word(),
    }

    response = authenticated_client["client"].patch(
        path=f"{valid_url}{product.id}", data=new_product_data
    )

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_delete_products_reaches_204_after_successful_authentication(
    authenticated_client, product_factory
):
    product = product_factory.create()
    expected_status_code = 204

    response = authenticated_client["client"].delete(path=f"{valid_url}{product.id}")

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_get_products_should_return_proper_count_of_products(
    authenticated_client, product_factory
):
    products = [product_factory.create() for _ in range(5)]

    authenticated_client["client"].get(valid_url)
    products_count_db = Product.objects.all().count()

    assert products_count_db == len(products)


@pytest.mark.django_db
def test_get_products_on_invalid_url_should_return_status_code_404(
    authenticated_client,
):
    expected_status_code = 404
    invalid_url = "/api/v1/get_products/"

    response = authenticated_client["client"].get(path=invalid_url)

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_get_specific_product_returns_product(authenticated_client, product_factory):
    product = product_factory.create()
    expected_status_code = 200

    response = authenticated_client["client"].get(path=f"{valid_url}{product.id}")

    assert response.status_code == expected_status_code
    assert response.data["id"] == product.id
    assert response.data["name"] == product.name
    assert response.data["category"] == product.category.id


@pytest.mark.django_db
def test_get_specific_product_returns_404_when_no_product(authenticated_client):
    invalid_product_id = 1
    expected_status_code = 404
    expected_message = "Not found."

    response = authenticated_client["client"].get(
        path=f"{valid_url}{invalid_product_id}"
    )

    assert response.status_code == expected_status_code
    assert str(response.data["detail"]) == expected_message


@pytest.mark.django_db
def test_post_with_proper_data_creates_new_product(
    authenticated_client, category_factory
):
    category = category_factory.create()
    new_product_data = {
        "name": fake.word(),
        "category": category.id,
    }
    expected_status_code = 201

    response = authenticated_client["client"].post(
        path=valid_url, data=new_product_data
    )
    product_db = Product.objects.all().first()

    assert response.status_code == expected_status_code
    assert product_db.name == new_product_data["name"]
    assert product_db.category.id == new_product_data["category"]


@pytest.mark.django_db
def test_post_with_missing_name_reaches_status_code_400(
    authenticated_client, category_factory
):
    category = category_factory.create()
    new_product_data = {
        "category": category.id,
    }
    expected_status_code = 400
    expected_error_field = "name"
    expected_error_message = "This field is required."

    response = authenticated_client["client"].post(
        path=valid_url, data=new_product_data
    )

    assert response.status_code == expected_status_code
    assert expected_error_field in response.data
    assert str(response.data[expected_error_field][0]) == expected_error_message


@pytest.mark.django_db
def test_post_with_missing_category_reaches_status_code_400(authenticated_client):
    new_product_data = {
        "name": fake.word(),
    }
    expected_status_code = 400
    expected_error_field = "category"
    expected_error_message = "This field is required."

    response = authenticated_client["client"].post(
        path=valid_url, data=new_product_data
    )

    assert response.status_code == expected_status_code
    assert expected_error_field in response.data
    assert str(response.data[expected_error_field][0]) == expected_error_message


@pytest.mark.django_db
def test_post_with_missing_all_the_data_reaches_status_code_400(authenticated_client):
    new_product_data = {}
    expected_status_code = 400
    expected_error_fields = ("category", "name")
    expected_error_message = "This field is required."

    response = authenticated_client["client"].post(
        path=valid_url, data=new_product_data
    )

    assert response.status_code == expected_status_code
    for field, details in response.data.items():
        assert field in expected_error_fields
        assert expected_error_message in details[0]


@pytest.mark.django_db
def test_patch_updates_product_name(authenticated_client, product_factory):
    product = product_factory.create()
    updated_product_data = {
        "name": fake.word(),
    }
    expected_status_code = 200

    response = authenticated_client["client"].patch(
        path=f"{valid_url}{product.id}", data=updated_product_data
    )

    assert response.status_code == expected_status_code
    assert response.data["name"] == updated_product_data["name"]


@pytest.mark.django_db
def test_patch_updates_product_category(
    authenticated_client, product_factory, category_factory
):
    product = product_factory.create()
    category = category_factory.create()
    updated_product_data = {
        "category": category.id,
    }
    expected_status_code = 200

    response = authenticated_client["client"].patch(
        path=f"{valid_url}{product.id}", data=updated_product_data
    )

    assert response.status_code == expected_status_code
    assert response.data["category"] == updated_product_data["category"]


@pytest.mark.django_db
def test_patch_updates_product_data(
    authenticated_client, product_factory, category_factory
):
    product = product_factory.create()
    category = category_factory.create()
    updated_product_data = {
        "name": fake.word(),
        "category": category.id,
    }
    expected_status_code = 200

    response = authenticated_client["client"].patch(
        path=f"{valid_url}{product.id}", data=updated_product_data
    )

    assert response.status_code == expected_status_code
    assert response.data["category"] == updated_product_data["category"]
    assert response.data["name"] == updated_product_data["name"]


@pytest.mark.django_db
def test_delete_deletes_product(authenticated_client, product_factory):
    products = [product_factory.create() for _ in range(5)]

    authenticated_client["client"].delete(path=f"{valid_url}{products[0].id}")
    products_count_db = Product.objects.all().count()

    assert products_count_db == len(products) - 1


@pytest.mark.django_db
def test_delete_specific_product_returns_404_when_no_product(authenticated_client):
    invalid_product_id = 1
    expected_status_code = 404
    expected_message = "Not found."

    response = authenticated_client["client"].delete(
        path=f"{valid_url}{invalid_product_id}"
    )

    assert response.status_code == expected_status_code
    assert str(response.data["detail"]) == expected_message
