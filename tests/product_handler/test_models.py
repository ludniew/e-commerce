import pytest
from products_handler.models import Product


@pytest.mark.django_db
def test_product_created(product_factory):
    product = product_factory.create()
    products_db = Product.objects.all()
    product_db = products_db.get(pk=product.pk)

    assert products_db.count() == 1
    assert product_db.name == product.name
    assert product_db.category == product.category


@pytest.mark.django_db
def test_str_product_should_return_product_name(product_factory):
    product = product_factory.create()
    product_db = Product.objects.all().get(pk=product.pk)

    assert str(product_db) == product.name
