from django.contrib.auth.models import User
from rest_framework.test import APIClient

import pytest
from faker import Faker

from ordering_handler.models import Order

valid_url = "/api/v1/accounts/register/"
fake = Faker()


@pytest.mark.django_db
def test_post_with_valid_url_returns_status_code_201(client):
    expected_status_code = 201
    valid_data = {
        "username": fake.user_name(),
        "password": fake.password(),
        "email": fake.email(),
    }

    response = client.post(path=valid_url, data=valid_data)

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_post_with_invalid_url_reaches_status_code_404(client):
    expected_status_code = 404
    invalid_path = "/api/v1/account/register/"

    response = client.post(path=invalid_path)

    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_post_with_valid_data_creates_user_properly(client):
    valid_data = {
        "username": fake.user_name(),
        "password": fake.password(),
        "email": fake.email(),
    }

    response = client.post(path=valid_url, data=valid_data)
    db_user = User.objects.all().first()
    db_order = Order.objects.get(account__user=db_user)

    assert "email" in response.data
    assert "username" in response.data
    assert "password" not in response.data
    assert db_user.username == valid_data["username"]
    assert db_user.email == valid_data["email"]
    assert db_user.password
    assert db_order


@pytest.mark.django_db
def test_post_with_username_missing_should_reach_status_code_(client):
    expected_status_code = 400
    expected_error_message = "This field is required."
    invalid_data = {"password": fake.password(), "email": fake.email()}

    response = client.post(path=valid_url, data=invalid_data)

    assert response.status_code == expected_status_code
    assert str(response.data["username"][0]) == expected_error_message


@pytest.mark.django_db
def test_post_with_password_missing_should_reach_status_code_(client):
    expected_status_code = 400
    expected_error_message = "This field is required."
    invalid_data = {"username": fake.user_name(), "email": fake.email()}

    response = client.post(path=valid_url, data=invalid_data)

    assert response.status_code == expected_status_code
    assert str(response.data["password"][0]) == expected_error_message


@pytest.mark.django_db
def test_get_method_should_be_not_allowed(client):
    expected_status_code = 405

    response = client.get(path=valid_url)

    assert response.status_code == expected_status_code
    assert response.data["detail"] == 'Method "GET" not allowed.'
