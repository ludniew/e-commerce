import pytest
from django.contrib.auth.models import User


@pytest.mark.django_db
def test_user_created(user_factory):
    user = user_factory.create()
    users_db = User.objects.all()
    user_db = users_db.get(pk=user.pk)

    assert users_db.count() == 1
    assert user_db.username == user.username
    assert user_db.password == user.password
    assert user_db.email == user.email
