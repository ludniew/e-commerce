import pytest
from rest_framework.test import APIClient
from pytest_factoryboy import register
from tests.factories import (
    UserFactory,
    ProductFactory,
    CategoryFactory,
    OrderFactory,
    AccountFactory,
    OrderRequestFactory,
)


register(UserFactory)
register(ProductFactory)
register(CategoryFactory)
register(OrderFactory)
register(AccountFactory)
register(OrderRequestFactory)


@pytest.fixture
def client():
    return APIClient()


@pytest.fixture
# @pytest.mark.django_db
def authenticated_client(user_factory):
    user = user_factory.create()
    client = APIClient()
    client.force_authenticate(user=user)
    return {"client": client, "user": user}
