from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)


api_url = "api/v1"

urlpatterns = [
    path("admin/", admin.site.urls),
    path(f"{api_url}/", include("products_handler.urls")),
    path(f"{api_url}/", include("categories_handler.urls")),
    path(f"{api_url}/", include("accounts.urls")),
    path(f"{api_url}/", include("ordering_handler.urls")),
    path(f"{api_url}/accounts/", include("accounts.urls")),
    path(f"{api_url}/token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path(f"{api_url}/token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    # path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    # path('api_v1/', include('rest_framework.urls', namespace='rest_framework'))
]
