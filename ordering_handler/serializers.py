from rest_framework import serializers

from accounts.serializers import UserSerializer
from ordering_handler.models import OrderRequest, Order


class OrderRequestCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderRequest
        fields = [
            "product",
        ]


class OrderDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = [
            "id",
            "order_products",
            "order_date",
            "additional_info",
        ]
