from django.http import JsonResponse
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import View
from ordering_handler.models import OrderRequest, Order
from ordering_handler.serializers import (
    OrderRequestCreateSerializer,
    OrderDetailsSerializer,
)


class OrderRequestCreateView(generics.CreateAPIView):
    queryset = OrderRequest.objects.all()

    def create(self, request, *args, **kwargs):
        user_order = Order.objects.get(account__user__id=request.user.id)

        serializer = OrderRequestCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        order_request = OrderRequest.objects.create(
            order=user_order, product=serializer.validated_data["product"]
        )

        data = {
            "order_id": user_order.id,
            "order_request_id": order_request.id,
            "product_id": serializer.data["product"],
            "user_id": request.user.id,
        }

        return Response(data=data, status=status.HTTP_201_CREATED)


class OrderRequestDestroyView(generics.RetrieveDestroyAPIView):
    queryset = OrderRequest.objects.all()


class OrderRetrieveView(generics.RetrieveAPIView):
    serializer_class = OrderDetailsSerializer
    queryset = Order.objects.all()


class OrderListView(generics.ListAPIView):
    serializer_class = OrderDetailsSerializer

    def get_queryset(self):
        return Order.objects.filter(account__user__id=self.request.user.id)


class OrderFinalize(View):
    def get(self, request, pk, *args, **kwargs):
        order = Order.objects.get(id=pk)
        order_requests = OrderRequest.objects.filter(order=order)
        products_ordered = [
            order_request.product.id for order_request in order_requests
        ]
        order_requests.delete()

        return JsonResponse(
            data={
                "operation": "Order finalize",
                "result": "Success",
                "products_ordered": products_ordered,
            },
            status=status.HTTP_200_OK,
        )
