from django.urls import path
from ordering_handler.views import (
    OrderRequestCreateView,
    OrderRetrieveView,
    OrderListView,
    OrderRequestDestroyView,
    OrderFinalize,
)


urlpatterns = [
    path("order/", OrderRequestCreateView.as_view(), name="make-order"),
    path("order/<int:pk>/", OrderRequestDestroyView.as_view(), name="delete-order"),
    path("orders/", OrderListView.as_view(), name="list-orders"),
    path("orders/<int:pk>/", OrderRetrieveView.as_view(), name="order-details"),
    path("orders/<int:pk>/finalize/", OrderFinalize.as_view(), name="finalize-order"),
]
