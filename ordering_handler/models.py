from django.db import models

from products_handler.models import Product
from accounts.models import Account


class Order(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    order_products = models.ManyToManyField(Product, through="OrderRequest")
    order_date = models.DateField()
    additional_info = models.CharField(max_length=200, blank=True, default="")

    def __str__(self):
        return f"Order(owner={self.account.user.id})"


class OrderRequest(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return f"OrderRequestt(id={self.id}), Order(id={self.order.id}), Product(id={self.product.id})"
