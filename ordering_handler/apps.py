from django.apps import AppConfig


class OrderingHandlerConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ordering_handler"
