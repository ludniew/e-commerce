from django.contrib import admin
from ordering_handler.models import Order, OrderRequest


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass


@admin.register(OrderRequest)
class OrderRequestAdmin(admin.ModelAdmin):
    pass
