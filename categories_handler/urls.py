from django.urls import path
from categories_handler.views import CategoryDetailView, CategoryListCreateView


urlpatterns = [
    path("categories/", CategoryListCreateView.as_view()),
    path("categories/<int:pk>", CategoryDetailView.as_view()),
]
