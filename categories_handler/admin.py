from django.contrib import admin
from categories_handler.models import Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass
