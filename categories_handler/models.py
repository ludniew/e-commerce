from django.db import models


class Category(models.Model):
    class Meta:
        verbose_name_plural = "Categories"

    name = models.CharField(max_length=20)
    parent_category = models.ForeignKey(
        "self", on_delete=models.CASCADE, related_name="child", null=True, blank=True
    )

    def __str__(self):
        return self.name
