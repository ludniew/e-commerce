from django.urls import path
from products_handler.views import ProductListCreateView, ProductDetailUpdateDestroyView


urlpatterns = [
    path("products/", ProductListCreateView.as_view()),
    path("products/<int:pk>", ProductDetailUpdateDestroyView.as_view()),
]
