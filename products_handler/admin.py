from django.contrib import admin
from products_handler.models import Product


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass
