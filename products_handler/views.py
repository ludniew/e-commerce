from rest_framework import generics
from rest_framework.exceptions import JsonResponse
from rest_framework.status import HTTP_204_NO_CONTENT
from products_handler.models import Product
from products_handler.serializers import ProductSerializer


class ProductListCreateView(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get(self, request, *args, **kwargas):
        return super().get(request, args, kwargas)


class ProductDetailUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    lookup_field = "pk"
